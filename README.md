# "Hello, World!" in Python

## Code

File: `hello_world.py`

Content:

```python
#!/usr/bin/env python
"""
  Example Application, which prints: "Hello, World!"
"""

print(f"Hello, World!")
```

## Usage
```bash
python hello_world.py
```

## Output
```
Hello, World!
```